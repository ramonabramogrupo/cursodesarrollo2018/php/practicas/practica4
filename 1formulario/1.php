<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <div>
            <form method="get">
                <div>
                    <label for="numero1">Numero1</label>
                    <input type="number" name="numero[]" id="numero1" value="2">
                </div>
                <div>
                    <label for="numero2">Numero2</label>
                    <input type="number" name="numero[]" id="numero2" value="3">
                </div>
                <div>
                    <label for="numero3">Numero3</label>
                    <input type="number" name="numero[]" id="numero3" value="4">
                </div>
                <button name="enviar">Enviar</button>
            </form>
        </div>
        <?php
        if (isset($_REQUEST["enviar"])) {
            $numeros=$_REQUEST["numero"];
            
            $ordenado = $numeros;
            sort($ordenado);

            foreach ($ordenado as $valores) {
                echo "$valores ";
            }

            if ($ordenado == $numeros) {
                echo "Los datos estaban ordenados";
            } else {
                echo "Los datos no estaban ordenados";
            }
        }
        ?>
    </body>
</html>
