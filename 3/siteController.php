<?php
        
    function actionFormulario(){
        return[
            "vista"=>"formulario.php"
        ];
    }

    function actionMedia(){
        $datos=$_REQUEST['numeros'];
        /**
         * compruebo si es array o string para saber de donde vengo
         */
        if(gettype($datos)=="string"){
            $datos= explode("-", $_REQUEST['numeros']);    
        }
        
        $resultado=0;
        foreach ($datos as $v){
            $resultado+=$v;
        }
        $resultado/=count($datos);
        $datos=implode("-",$datos);
        /*
         * antes de mandar a la vista los numeros los convierto en
         * un string
         */
        
        return[
            'vista'=>'resultado.php',
            'mensaje'=>'La media calculada es:',
            'resultado'=>$resultado,
            'numeros'=>$datos,
        ];
    }
    
    function actionModa(){
        return[
            
        ];
    }
    
    function actionMediana(){
        return[
            
        ];
    }
    
    function actionDesviacion(){
        return[
            
        ];
    }

        if (isset($_REQUEST["boton"])) {
            switch ($_REQUEST["boton"]){
                case 'media':
                    $datos= actionMedia();
                    break;
                case 'moda':
                    $datos=actionModa();
                    break;
                case 'mediana':
                    $datos=actionMediana();
                    break;
                case 'desviacion':
                    $datos= actionDesviacion();
                    break;
                default:
                    $datos= actionFormulario();
            }
           
        }else{
                 $datos= actionFormulario();
        }


